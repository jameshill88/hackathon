/**
* TEMPLATE
*/
define( 'logic', [ 'player', 'world' ], function( MOD_Player, MOD_World ){

	var thisModule = (function(){


		function m_LookAction( subj ){

			var response = "notset";
			var player = MOD_Player.get();
			var rooms = MOD_World.getRooms();
			var items = MOD_World.getItems();


			// Direction subjects
			if( subj == "n" || subj == "e" || subj == "s" || subj == "w" ){

				switch( subj ){

					case "n":

						var dirContents = rooms[ player.location ].north;
						player.facing = "north";

						response = "You look north. You see";

						break;

					case "e":

						var dirContents = rooms[ player.location ].east;
						player.facing = "east";

						response = "You look east. You see";

						break;

					case "s":

						var dirContents = rooms[ player.location ].south;
						player.facing = "south";

						response = "You look south. You see";

						break;

					case "w":

						var dirContents = rooms[ player.location ].west;
						player.facing = "west";

						response = "You look west. You see";

						break;

					default:

						break;
				}


				// list the items in this location
				var itemCount = 0;
				for( var i = 0; i < dirContents.length; i++ ){

					response += " " + items[ dirContents[ i ] ].look;
					itemCount++;

					if( i < dirContents.length - 1 ){

						response += ",";

						if( i == dirContents.length - 2 ){

							response += "and ";
						}
					}
				}

				if( itemCount == 0 ){
					
					response += " nothing";
				}

				response += ".";

				MOD_Player.set( player );
			
			}else{ // Item subjects

				for( key in items ){

					if( items.hasOwnProperty( key )){

						if( key == subj ){

							var itemsInFacingDir = rooms[ player.location ][ player.facing ];
							var subjectInView = false;

							for( var i = 0; i < itemsInFacingDir.length; i++ ){

								if( itemsInFacingDir[ i ] == key ){

									subjectInView = true;
								}
							}

							if( subjectInView ){

								response = "You see " + items[ key ].look;
							}else{

								response = "You look to the " + player.facing + ", but you cannot see a " + items[ key ].subjName;
							}
						}
					}
				}
			}

			return response;
		}


		function m_SearchAction( subj ){

			var response = "notset";
			var player = MOD_Player.get();
			var rooms = MOD_World.getRooms();
			var items = MOD_World.getItems();


			// Direction subjects
			if( subj == "n" || subj == "e" || subj == "s" || subj == "w" ){

				response = "You cannot search a direction. Use the look command.";
				//response = "Ye can't be searchin a direction, me lad. Use the look command.";
			
			}else if( subj == "inventory" ){ // Player inventory

				response = "Searching in your bag, you find ";
				var itemCount = 0;

				for( var i = 0; i < player.items.length; i++ ){

					response += player.items[ i ].look;

					if( i < player.items.length - 1 ){

						response += ", ";

						if( i == player.items.length - 2 ){

							response += "and ";
						}
					}
				}

				if( itemCount == 0 ){

					response += " nothing";
				}

				response += ".";

				player.lastSearched = subj;

			}else{ // Item subjects

				for( key in items ){

					if( items.hasOwnProperty( key )){

						if( key == subj ){

							var itemsInFacingDir = rooms[ player.location ][ player.facing ];
							var subjectInView = false;

							for( var i = 0; i < itemsInFacingDir.length; i++ ){

								if( itemsInFacingDir[ i ] == key ){

									subjectInView = true;
								}
							}

							if( subjectInView ){

								response = "Searching the " + items[ key ].subjName + ", you find ";

								var itemCount = 0;

								if( items[ key ].hasOwnProperty( "contents" )){

									for( var i = 0; i < items[ key ].contents.length; i++ ){

										response += items[ items[ key ].contents[ i ] ].look;
										itemCount++;

										if( i < items[ key ].contents.length - 1 ){

											response += ", ";

											if( i == items[ key ].contents.length - 2 ){

												response += "and ";
											}
										}
									}
								}

								if( itemCount == 0 ){

									response += " nothing";
								}

								response += ".";
								
							}else{

								response = "There is no " + items[ key ].subjName + " to search.";
							}
						}
					}
				}

				player.lastSearched = subj;
			}

			MOD_Player.set( player );

			return response;
		}


		function m_TakeAction( subj ){

			var response = "notset";
			var player = MOD_Player.get();
			var rooms = MOD_World.getRooms();
			var items = MOD_World.getItems();


			// Direction subjects
			if( subj == "n" || subj == "e" || subj == "s" || subj == "w" ){

				response = "You cannot take a direction. Use the look command.";
				//response = "Ye can't be searchin a direction, me lad. Use the look command.";
			
			}else{ // Item subjects

				for( key in items ){

					if( items.hasOwnProperty( key )){

						if( key == subj ){

							var itemsInFacingDir = rooms[ player.location ][ player.facing ];
							var subjectInView = false;
							var searchedIn = "nowhere";
							var objSearched = {
								key: "notset",
								idx: 0
							};

							for( var i = 0; i < itemsInFacingDir.length; i++ ){

								if( itemsInFacingDir[ i ] == key ){

									subjectInView = true;
									searchedIn = "in the direction you are facing";
									objSearched.key = player.facing;
									objSearched.idx = i;
								}
							}

							if( player.lastSearched == "inventory" ){

								for( var i = 0; i < player.items.length; i++ ){

									if( player.items[ i ] == key ){

										subjectInView = true;
										searchedIn = "the items you are carrying";
										objSearched.key = "inventory";
										objSearched.idx = i;
									}
								}
							}else{

								if( items[ player.lastSearched ].hasOwnProperty( "contents" )){

									var itemsInLastSearchedLocation = items[ player.lastSearched ].contents;
								}

								searchedIn = "the " + items[ player.lastSearched ].subjName;

								for( var i = 0; i < itemsInLastSearchedLocation.length; i++ ){

									if( itemsInLastSearchedLocation[ i ] == key ){

										subjectInView = true;
										objSearched.key = player.lastSearched;
										objSearched.idx = i;
									}
								}
							}

							if( subjectInView ){

								// Remove the item from its current location and add it to the player's inventory
								if( objSearched.key == "north" ||
									objSearched.key == "east" ||
									objSearched.key == "south" ||
									objSearched.key == "west" ){

										var removed = rooms[ player.location ][ objSearched.key ].splice( objSearched.idx, 1 );
										player.items.push( removed[ 0 ] );
										
										response = items[ key ].take;

								}else if( objSearched.key == "inventory" ){

									response = "You already have the " + player.items[ key ].subjName + ".";
								}else{

									var removed = items[ objSearched.key ].contents.splice( objSearched.idx, 1 );
									player.items.push( removed[ 0 ] );
									
									response = items[ key ].take;
								}
							
							}else{

								response = "You search " + searchedIn + ", but there is no " + items[ key ].subjName + " to take.";
							}
						}
					}
				}
			}

			MOD_World.setRooms( rooms );
			MOD_World.setItems( items );
			MOD_Player.set( player );

			return response;
		}


		function m_DoInputActions( parsedInput ){


			var response = "";

			// Actions
			switch( parsedInput.action ){

				case "look":

					response = m_LookAction( parsedInput.subject );

					break;

				case "search":

					response = m_SearchAction( parsedInput.subject );

					break;

				case "take":

					response = m_TakeAction( parsedInput.subject );

					break;

				case "use":

					response = "not implemented yet.";

					break;

				case "go":

					response = "not implemented yet.";

					break;

				case "talk":

					response = "not implemented yet.";

					break;

				default:

					break;
			}

			return response;
		}


		// Interface
		return {
			doInputActions: m_DoInputActions
		};

	}());	

	return thisModule;
});
