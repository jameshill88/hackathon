/**
* TEMPLATE
*/
define( 'world', function(){

	var thisModule = (function(){


		var m_Rooms = {

			cabin: {
				north: [ "cabinDoorToAft" ],
				south: [ "southCabinWall" ],
				east: [ "playerChest", "pollyCage" ],
				west: [ "yourHammock" ]
			}
		};


		var m_Items = {

			cabinDoorToAft: {
				look: "A wooden door leading to the aft deck of the pirate ship",
				subjName: "door",
				locked: true,
				validActions: [ "look", "use" ],
				responses: [
					
				]
			},

			southCabinWall: {
				look: "The southern wall of your cabin",
				subjName: "wall",
				validActions: [ "look" ]
			},

			playerChest: {
				look: "The battered wooden treasure chest containing your belongings",
				subjName: "chest",
				validActions: [ "look", "use" ],
				contents: [
					"yourSword", "chestBooty", "aftDoorKey"
				]
			},

			yourSword: {
				look: "Your trusty steel sword, stained with the blood of many a scurvy sea dog",
				take: "You pick up your sword and slide it into your scabbard",
				subjName: "sword",
				validActions: [ "look", "take" ]
			},

			aftDoorKey: {
				look: "A rusty key",
				take: "You stash the key in your bag",
				subjName: "key",
				validActions: [ "look", "take" ]
			},

			chestBooty: {
				look: "A small amount of booty, looted from the pockets of your fallen victims",
				subjName: "booty",
				validActions: [ "look", "search" ]
			},

			pollyCage: {
				look: "Polly's bird cage",
				subjName: "cage",
				validActions: [ "look", "use" ],
				contents: [ "polly" ]
			},

			polly: {
				look: "Polly, your faithful parrot companion",
				take: "You whistle for polly, who flies to you and perches on your shoulder",
				subjName: "polly",
				validActions: [ "look", "talk", "take" ]
			},

			yourHammock: {
				look: "Your hammock, strung across the wall of the cabin",
				subjName: "hammock",
				validActions: [ "look", "use" ]
			}

		};



		// Interface
		return {
			getItems: function(){
				return m_Items;
			},
			setItems: function( items ){
				m_Items = items;
			},
			getRooms: function(){
				return m_Rooms;
			},
			setRooms: function( rooms ){
				m_Rooms = rooms;
			}
		};

	}());	

	return thisModule;
});
