/**
* TEMPLATE
*/
define( 'player', function(){

	var thisModule = (function(){

		
		var m_Player = {
			location: "cabin",
			facing: "north",
			health: 100,
			items: [],
			lastSearched: "inventory"
		};


		// Interface
		return {
			get: function(){
				return m_Player;
			},
			set: function( player ){
				m_Player = player;
			}
		};

	}());	

	return thisModule;
});
