/**
* TEMPLATE
*/
define( 'output', function(){

	var thisModule = (function(){


		// Test if speech synth available
		(function(){

			if( !(window.speechSynthesis) ){

				alert('speech synthesis API not available');
			}
		}());


		// Get voices
		var voices = [];
		var loadVoicesIvl = window.setInterval( function(){

			if( voices.length < 1 ){
				voices = window.speechSynthesis.getVoices();
			}else{
				window.clearInterval( loadVoicesIvl );
			}
		}, 30 );

		
		function m_SpeakOutput( output ){

			var msg = new SpeechSynthesisUtterance( output );

			msg.voice = voices[ 1 ];

			window.speechSynthesis.speak( msg );
		}


		// Interface
		return {
			speakOutput: m_SpeakOutput
		};

	}());	

	return thisModule;
});
