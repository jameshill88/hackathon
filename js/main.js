/**
* MAIN
*/
require( [ 'jquery', 'world', 'input', 'logic', 'output' ], function( $, MOD_World, MOD_Input, MOD_Logic, MOD_Output ){

	$(document).ready(function(){


		// Speak button
		$( '#speak' ).on( 'click', function(){

			var parsedInput = MOD_Input.handleInput();

			if( parsedInput.validAction ){

				var resp = MOD_Logic.doInputActions( parsedInput );
				MOD_Output.speakOutput( resp );
			}else{

				console.log( 'invalid action or subject; not continuing')
			}
		});

	});
});
