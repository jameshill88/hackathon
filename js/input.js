/**
* TEMPLATE
*/
define( 'input', [ 'jquery', 'world' ], function( $, MOD_World ){

	var thisModule = (function(){

		
		var m_$input = $( '#tospeak' );

		var m_Actions = {
			look: "look",
			search: "search",
			use: "use",
			go: "go",
			take: "take",
			talk: "talk"
		};


		function m_GetInput(){

			return m_$input.val();
		}


		function m_GetSubject( str ){

			var itemList = MOD_World.getItems();

			var subj = "notset";

			switch( str ){

				case "north":

					subj = "n";

					break;

				case "east":

					subj = "e";

					break;

				case "south":

					subj = "s";

					break;

				case "west":

					subj = "w";

					break;

				case "inventory":

					subj = "inventory";

					break;

				case "self":

					subj = "inventory";

					break;

				case "items":

					subj = "inventory";

					break;

				default:

					for( key in itemList ){

						if( itemList.hasOwnProperty( key ) ){

							if( itemList[ key ].subjName == str ){

								subj = key;
							}
						}
					}

					break;
			}

			return subj;
		}

		function m_IsValidAction( action, subject ){

			var itemList = MOD_World.getItems();

			for( key in itemList ){

				if( itemList.hasOwnProperty( key )){

					for( var i = 0; i < itemList[ key ].validActions.length; i++ ){

						if( itemList[ key ].validActions[ i ] == action ){

							return true;
						}
					}
				}
			}

			console.log( 'not a valid action' );
			return false;
		}


		function m_ParseInput( input ){

			var inputParsed = {
				action: "notset",
				subject: "notset",
				validAction: false
			};

			var words = input.split( " " );

			if( words.length < 2 ){
				
				console.log( 'input too short!' );
				return inputParsed;
			}

			// Find command action
			if( m_Actions.hasOwnProperty( words[ 0 ] ) ){

				inputParsed.action = m_Actions[ words[ 0 ] ];
			}else{

				console.log( 'word 1 must be a valid action.' );
			}

		
			inputParsed.subject = m_GetSubject( words [ 1 ] );
			
			if( inputParsed.subject == "notset" ){

				console.log( 'word 2 must be a valid subject.' );
			}
			

			return inputParsed;
		}


		function m_HandleInput(){

			var inputRaw = m_GetInput();
			var inputParsed = m_ParseInput( inputRaw );

			if( inputParsed.action == "notset" || inputParsed.subject == "notset" ){

				return false;
			}

			inputParsed.validAction = m_IsValidAction( inputParsed.action, inputParsed.subject );

			return inputParsed;
		}


		// Interface
		return {
			handleInput: m_HandleInput
		};

	}());	

	return thisModule;
});
